package com.example.tp2_exo1;

// SensorAdapter.java

import android.hardware.Sensor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SensorAdapter extends RecyclerView.Adapter<SensorAdapter.SensorViewHolder> {

    private List<Sensor> sensorList;

    public SensorAdapter(List<Sensor> sensorList) {
        this.sensorList = sensorList;
    }

    @NonNull
    @Override
    public SensorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sensor_item, parent, false);
        return new SensorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SensorViewHolder holder, int position) {
        Sensor sensor = sensorList.get(position);
        holder.sensorName.setText(sensor.getName());
        holder.sensorType.setText("Type: " + sensor.getType());
        holder.sensorAccuracy.setText("Accuracy: " + sensor.getResolution());
    }

    @Override
    public int getItemCount() {
        return sensorList.size();
    }

    public static class SensorViewHolder extends RecyclerView.ViewHolder {

        TextView sensorName;
        TextView sensorType;
        TextView sensorAccuracy;

        public SensorViewHolder(View itemView) {
            super(itemView);
            sensorName = itemView.findViewById(R.id.sensorName);
            sensorType = itemView.findViewById(R.id.sensorType);
            sensorAccuracy = itemView.findViewById(R.id.sensorAccuracy);
        }
    }
}

