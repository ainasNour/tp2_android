package com.example.tp2_exo1;

/*
import android.content.Context;
import android.hardware.Sensor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class SensorListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Sensor> mSensors;

    public SensorListAdapter(Context context, List<Sensor> sensors) {
        mContext = context;
        mSensors = sensors;
    }

    @Override
    public int getCount() {
        return mSensors.size();
    }

    @Override
    public Object getItem(int position) {
        return mSensors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.sensor_list_item, parent, false);
        }

        Sensor sensor = mSensors.get(position);

        TextView sensorNameTextView = (TextView) convertView.findViewById(R.id.sensor_name_text_view);
        TextView sensorTypeTextView = (TextView) convertView.findViewById(R.id.sensor_type_text_view);
        TextView sensorVendorTextView = (TextView) convertView.findViewById(R.id.sensor_vendor_text_view);

        sensorNameTextView.setText(sensor.getName());
        sensorTypeTextView.setText("Type : " + sensor.getStringType());
        sensorVendorTextView.setText("Fabricant : " + sensor.getVendor());

        return convertView;
    }
}
*/