package com.example.tp2_exo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Récupération de la vue qui contiendra la liste des capteurs
        LinearLayout sensorListLayout = findViewById(R.id.sensor_list);

        // Récupération du bouton pour afficher la liste des capteurs
        findViewById(R.id.btn_get_sensors).setOnClickListener(view -> {
            // Récupération des capteurs disponibles
            List<Sensor> sensorList = getSensorList();

            // Affichage de la liste des capteurs
            for (Sensor sensor : sensorList) {
                // Création d'un TextView pour afficher le nom du capteur
                TextView sensorView = new TextView(MainActivity.this);
                sensorView.setText(sensor.getName());

                // Ajout du TextView à la vue contenant la liste des capteurs
                sensorListLayout.addView(sensorView);
            }
        });
    }

    private List<Sensor> getSensorList() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);

        // Parcours de la liste des capteurs et affichage de leur nom dans la console de log
        for (Sensor sensor : sensorList) {
            Log.d("SENSOR", sensor.getName());
        }

        return sensorList;
    }
}