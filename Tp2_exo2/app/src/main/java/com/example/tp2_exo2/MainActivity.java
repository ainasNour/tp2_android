package com.example.tp2_exo2;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    private TextView accelerometerStatusTextView;
    private TextView gyroscopeStatusTextView;
    private TextView proximityStatusTextView;
    private TextView ambientTemperatureStatusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        accelerometerStatusTextView = findViewById(R.id.accelerometer_status_text_view);
        gyroscopeStatusTextView = findViewById(R.id.gyroscope_status_text_view);
        proximityStatusTextView = findViewById(R.id.proximity_status_text_view);
        ambientTemperatureStatusTextView = findViewById(R.id.ambient_temperature_status_text_view);

        // Détecter la disponibilité des capteurs et mettre à jour les vues en conséquence
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometerSensor == null) {
            accelerometerStatusTextView.setText("Accéléromètre indisponible");
        }

        Sensor gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if (gyroscopeSensor == null) {
            gyroscopeStatusTextView.setText("Gyroscope indisponible");
        }

        Sensor proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (proximitySensor == null) {
            proximityStatusTextView.setText("Capteur de proximité indisponible");
        }

        Sensor ambientTemperatureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if (ambientTemperatureSensor == null) {
            ambientTemperatureStatusTextView.setText("Capteur de température ambiante indisponible");
        }
    }
}
