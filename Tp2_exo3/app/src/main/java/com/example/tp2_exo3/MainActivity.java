package com.example.tp2_exo3;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private static final int COLOR_GREEN = Color.parseColor("#00FF00");
    private static final int COLOR_BLACK = Color.parseColor("#000000");
    private static final int COLOR_RED = Color.parseColor("#FF0000");
    private static final long UPDATE_INTERVAL = 100; // Intervalle de temps en millisecondes entre chaque mise à jour de la couleur de fond
    private long lastUpdate = 0; // Temps de la dernière mise à jour

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialisation du SensorManager et de l'accéléromètre
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Enregistrement de l'accéléromètre
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Désenregistrement de l'accéléromètre
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Vérification du temps depuis la dernière mise à jour
        long now = System.currentTimeMillis();
        if (now - lastUpdate < UPDATE_INTERVAL) {
            return;
        }
        lastUpdate = now;

        // Récupération des valeurs de l'accéléromètre
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        // Calcul de la valeur moyenne
        float avg = (Math.abs(x) + Math.abs(y) + Math.abs(z)) / 3;

        // Choix de la couleur en fonction de la valeur moyenne
        int color;
        if (avg < 4) {
            color = COLOR_GREEN;
        } else if (avg >=5 && avg<7) {
            color = COLOR_BLACK;
        } else {
            color = COLOR_RED;
        }

        // Changement de la couleur de fond de l'écran
        getWindow().getDecorView().setBackgroundColor(color);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Ne rien faire
    }
}
