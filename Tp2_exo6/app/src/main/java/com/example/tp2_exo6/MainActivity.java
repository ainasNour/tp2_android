package com.example.tp2_exo6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor proximitySensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (proximitySensor != null) {
            sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }
    @Override
    protected void onStop() {
        super.onStop();

        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Ne rien faire
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float distance = event.values[0];

        if (distance < proximitySensor.getMaximumRange()) {
            // L'objet est proche
            ImageView image = findViewById(R.id.image);
            image.setImageResource(R.drawable.proche);

            TextView status = findViewById(R.id.status);
            status.setText("Proche");
        } else {
            // L'objet est loin
            ImageView image = findViewById(R.id.image);
            image.setImageResource(R.drawable.loin);

            TextView status = findViewById(R.id.status);
            status.setText("Loin");
        }
    }


    // ...
}
