package com.example.tp2_exo4;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private TextView directionText;

    private static final int COLOR_LEFT = Color.parseColor("#FF0000");    // Rouge
    private static final int COLOR_RIGHT = Color.parseColor("#0000FF");   // Bleu
    private static final int COLOR_UP = Color.parseColor("#00FF00");      // Vert
    private static final int COLOR_DOWN = Color.parseColor("#FFFF00");    // Jaune

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        directionText = findViewById(R.id.directionText);

        // Initialisation du SensorManager et de l'accéléromètre
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Enregistrement de l'accéléromètre
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Désenregistrement de l'accéléromètre
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Récupération des valeurs de l'accéléromètre
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        // Calcul de la valeur absolue pour chaque axe
        float absX = Math.abs(x);
        float absY = Math.abs(y);
        float absZ = Math.abs(z);

        // Choix de la direction en fonction des valeurs absolues
        int directionColor;
        String directionTextString;
        if (absX > absY && absX > absZ) {
            if (x > 0) {
               // directionColor = COLOR_LEFT;
                directionTextString = "Gauche";
            } else {
              //  directionColor = COLOR_RIGHT;
                directionTextString = "Droite";
            }
        } else if (absY > absX && absY > absZ) {
            if (y > 0) {
              //  directionColor = COLOR_DOWN;
                directionTextString = "Haut";
            } else {
                //directionColor = COLOR_UP;
                directionTextString = "Bas";
            }
        } else {
            directionColor = Color.WHITE;
            directionTextString = "";
        }

        // Changement de la couleur et du texte de la direction
       // directionText.setTextColor(directionColor);
        directionText.setText(directionTextString);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Ne rien faire
    }
}
